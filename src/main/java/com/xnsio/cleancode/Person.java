package com.xnsio.cleancode;

import java.util.HashMap;
import java.util.Map;

public class Person {

    private String name;
    private Map<Integer, Map<Hour, String>> calender = new HashMap<>();

    Person(String name) {
        this.name = name;
    }

    public void setCalender(Map<Integer, Map<Hour, String>> calender) {
        this.calender = calender;
    }

    public boolean isFreeAt(int date, Hour hour) {
        if(calender.containsKey(date)) {
            if(calender.get(date).containsKey(hour)) {
                if (calender.get(date).get(hour).isEmpty()) return true;
                else return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    public void addToCalender(int date, Hour hour, String desc) {
        if(calender.containsKey(date)) {
            Map<Hour, String> dayCalender = calender.get(date);
            dayCalender.put(hour, desc);
        } else {
            Map<Hour, String> dayCalender = new HashMap<>();
            dayCalender.put(hour, desc);
            calender.put(date, dayCalender);
        }
    }

    enum Hour {
        EIGHT, NINE, TEN, ELEVEN, TWELVE, ONE, TWO, THREE, FOUR, FIVE
    }
}
