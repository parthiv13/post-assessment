package com.xnsio.cleancode;

public class MeetingAssistant {

    public String getFirstAvailableTime(Person person1, Person person2, int endDate, Person.Hour endHour) {
        for (int day=1; day<=endDate; day++) {
            for (Person.Hour hour : (Person.Hour.values())) {
                if((hour.compareTo(endHour) > 0))
                    return "No available time-slot";
                else if((person1.isFreeAt(day, hour) && (person2.isFreeAt(day, hour)))) {
                    return hour.toString();
                }
            }
        }
        return "No available time-slot";
    }
}
