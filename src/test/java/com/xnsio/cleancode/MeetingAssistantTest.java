package com.xnsio.cleancode;

import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

public class MeetingAssistantTest {

    private MeetingAssistant meetingAssistant = new MeetingAssistant();

    @Test
    public void busyBeeIsAlwaysBusy() {
        String firstAvailableTime = meetingAssistant.getFirstAvailableTime(busyBee(), new Person("free"), 1, Person.Hour.FIVE);
        assertEquals("No available time-slot", firstAvailableTime);
    }

    @Test
    public void bothAreFreeAtTheFirstHour() {
        String firstAvailableTime = meetingAssistant.getFirstAvailableTime(new Person("freeGuy1"), new Person("freeGuy2"), 10, Person.Hour.FIVE);
        assertEquals("EIGHT", firstAvailableTime);
    }

    @Test
    public void bothAreFreeSomeWhereInBetween() {
        Person person1 = new Person("FreeAtDay1Hour5");
        Person person2 = new Person("FreeAtDay2Hour5");
        Map<Person.Hour, String> dayCalender1 = new HashMap<>();
        Map<Integer, Map<Person.Hour, String>> calender1 = new HashMap<>();
        dayCalender1.put(Person.Hour.EIGHT, "busy");
        dayCalender1.put(Person.Hour.NINE, "busy");
        dayCalender1.put(Person.Hour.ELEVEN, "busy");
        dayCalender1.put(Person.Hour.THREE, "busy");
        calender1.put(1, dayCalender1);
        person1.setCalender(calender1);

        Map<Person.Hour, String> dayCalender2 = new HashMap<>();
        Map<Integer, Map<Person.Hour, String>> calender2 = new HashMap<>();
        dayCalender2.put(Person.Hour.TEN, "busy");
        dayCalender2.put(Person.Hour.TWELVE, "busy");
        dayCalender2.put(Person.Hour.ONE, "busy");
        dayCalender2.put(Person.Hour.TWO, "busy");
        dayCalender2.put(Person.Hour.FIVE, "busy");
        calender2.put(1, dayCalender2);
        person2.setCalender(calender2);
        String firsAvailableTime = meetingAssistant.getFirstAvailableTime(person1, person2, 2, Person.Hour.FIVE);
        assertEquals("FOUR", firsAvailableTime);
    }

    @Test
    public void calenderAlreadySaysMeetEachOther() {

    }

    private Person busyBee() {
        Person busyBee = new Person("busyBee");
        Map<Person.Hour, String> dayCalender = new HashMap<>();
        Map<Integer, Map<Person.Hour, String>> calender = new HashMap<>();
        dayCalender.put(Person.Hour.EIGHT, "busy");
        dayCalender.put(Person.Hour.NINE, "busy");
        dayCalender.put(Person.Hour.TEN, "busy");
        dayCalender.put(Person.Hour.ELEVEN, "busy");
        dayCalender.put(Person.Hour.TWELVE, "busy");
        dayCalender.put(Person.Hour.ONE, "busy");
        dayCalender.put(Person.Hour.TWO, "busy");
        dayCalender.put(Person.Hour.THREE, "busy");
        dayCalender.put(Person.Hour.FOUR, "busy");
        dayCalender.put(Person.Hour.FIVE, "busy");
        calender.put(1, dayCalender);
        calender.put(2, dayCalender);
        busyBee.setCalender(calender);
        return busyBee;
    }
}